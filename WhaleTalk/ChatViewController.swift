//
//  ViewController.swift
//  WhaleTalk
//
//  Created by Elliott Diaz on 5/28/16.
//  Copyright © 2016 Elliott Diaz. All rights reserved.
//

import UIKit

class ChatViewController: UIViewController
{
    // Create UITableView Object Private To The Class
    
    private let tableView       = UITableView()
    private let newMessageField = UITextView ()
    private let cellIdentifier  = "Cell"
    private var messages        = [Message]()
    
    private var bottomConstraint : NSLayoutConstraint!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        tableView.dataSource = self
        tableView.delegate   = self
        
        tableView.estimatedRowHeight = 44
        
        tableView.registerClass( ChatCell.self, forCellReuseIdentifier: cellIdentifier )
        
        tableView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview( tableView )
        
        var localIncoming = true
        for i in 0...10
        {
            let m  = Message()
           // m.text = String(i)
            m.text = "This is a longer Text Yoooo"
            m.incoming    = localIncoming
            localIncoming = !localIncoming
            messages.append(m)
        }
        
        let newMessageArea = UIView()
        newMessageArea.backgroundColor                           = UIColor.grayColor()
        newMessageArea.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview( newMessageArea )
        
        newMessageField.translatesAutoresizingMaskIntoConstraints = false
        newMessageField.scrollEnabled                             = false
        newMessageArea .addSubview( newMessageField)
        
        let sendButton = UIButton()
        sendButton.translatesAutoresizingMaskIntoConstraints = false
        sendButton.setTitle                               ( "Send", forState: .Normal     )
        sendButton.setContentHuggingPriority              ( 251   , forAxis : .Horizontal )
        sendButton.setContentCompressionResistancePriority( 751   , forAxis : .Horizontal )
        newMessageArea.addSubview                         ( sendButton                    )
        
        bottomConstraint        = newMessageArea.bottomAnchor.constraintEqualToAnchor( view.bottomAnchor )
        bottomConstraint.active = true
        
        let newMessageAreaConstraints: [ NSLayoutConstraint ] =
        [
            newMessageArea .leadingAnchor .constraintEqualToAnchor( view.leadingAnchor                            ),
            newMessageArea .trailingAnchor.constraintEqualToAnchor( view.trailingAnchor                           ),
            newMessageField.leadingAnchor .constraintEqualToAnchor( newMessageArea .leadingAnchor , constant: 10  ),
            newMessageField.centerYAnchor .constraintEqualToAnchor( newMessageArea .centerYAnchor                 ),
            sendButton     .trailingAnchor.constraintEqualToAnchor( newMessageArea .trailingAnchor, constant: -10 ),
            newMessageField.trailingAnchor.constraintEqualToAnchor( sendButton     .leadingAnchor , constant: -10 ),
            sendButton     .centerYAnchor .constraintEqualToAnchor( newMessageField.centerYAnchor                 ),
            newMessageArea .heightAnchor  .constraintEqualToAnchor( newMessageField.heightAnchor  , constant: 20  )
        ]
        
        NSLayoutConstraint.activateConstraints( newMessageAreaConstraints )
        
        // Set Up Constraints For Table View To Take Up Size Of The Screen

        let tableViewConstraints: [NSLayoutConstraint] =
        [
            tableView.topAnchor     .constraintEqualToAnchor( view.topAnchor           ),
            tableView.leadingAnchor .constraintEqualToAnchor( view.leadingAnchor       ),
            tableView.trailingAnchor.constraintEqualToAnchor( view.trailingAnchor      ),
            tableView.bottomAnchor  .constraintEqualToAnchor( newMessageArea.topAnchor )
        ]
        NSLayoutConstraint.activateConstraints( tableViewConstraints )
        
        NSNotificationCenter.defaultCenter().addObserver( self, selector: #selector( ChatViewController.keyboardWillShow(_:)), name: UIKeyboardWillShowNotification, object: nil )
        
        NSNotificationCenter.defaultCenter().addObserver( self, selector: #selector(ChatViewController.keyboardWillHide(_:)) , name: UIKeyboardWillHideNotification, object: nil )
        
        let tapGestureRecognizer                  = UITapGestureRecognizer( target: self, action: #selector(ChatViewController.handleSingleTap(_:)) )
        tapGestureRecognizer.numberOfTapsRequired = 1
        view.addGestureRecognizer( tapGestureRecognizer )
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func handleSingleTap( tap: UITapGestureRecognizer )
    {
        view.endEditing( true )
    }
    
    func keyboardWillShow( notification: NSNotification )
    {
        updateBottomContraint( notification )
    }
    
    func keyboardWillHide( notification: NSNotification )
    {
        updateBottomContraint( notification )
    }
    
    func updateBottomContraint( notification: NSNotification )
    {
        if let userInfo       = notification.userInfo,
            frame             = userInfo[UIKeyboardFrameEndUserInfoKey]?.CGRectValue,
            animationDuration = userInfo[UIKeyboardAnimationDurationUserInfoKey]?.doubleValue
        {
            let newFrame              = view.convertRect(frame, fromView: (UIApplication.sharedApplication().delegate?.window)!)
            bottomConstraint.constant = newFrame.origin.y - CGRectGetHeight( view.frame )
            UIView.animateWithDuration( animationDuration, animations:
            {
                    self.view.layoutIfNeeded()
            })
        }
    }
}

extension ChatViewController: UITableViewDataSource
{
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return messages.count
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let cell    = tableView.dequeueReusableCellWithIdentifier(cellIdentifier, forIndexPath: indexPath) as! ChatCell
        let message = messages[indexPath.row]
        cell.messageLabel.text = message.text
        cell.incoming(message.incoming)
        cell.separatorInset = UIEdgeInsetsMake( 0, tableView.bounds.size.width, 0, 0 ) // remove cell line seperator
        return cell
    }
}

extension ChatViewController: UITableViewDelegate
{
    func tableView(tableView: UITableView, shouldHighlightRowAtIndexPath indexPath: NSIndexPath) -> Bool
    {
        return false
    }
}


































































































































































